---
title: "I'm wrong"
description: "Turns out somehting isn't right."
date: "2016-10-16"
categories: 
    - "template"
    - "cant delete"
    - "why tho"
---

## Interesting find

So I tried deleting all the content files(sample posts) included in this template and one file still appears eventhough it is not referencing a file now.

From what I can figure out from the code: it looks at files in the content/posts section and generate a link to it and format it.

But now that it's gone....why does it still generate a link to it?